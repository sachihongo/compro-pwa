import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-galery-detail',
  templateUrl: './galery-detail.page.html',
  styleUrls: ['./galery-detail.page.scss'],
})
export class GaleryDetailPage implements OnInit {
  image:any;

  constructor(private router:ActivatedRoute) { 
    this.image = this.router.snapshot.paramMap.get('image');
  }

  ngOnInit() {
  }

}
