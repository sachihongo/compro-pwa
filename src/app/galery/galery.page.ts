import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-galery',
  templateUrl: './galery.page.html',
  styleUrls: ['./galery.page.scss'],
})
export class GaleryPage implements OnInit {
  

  photo1 = "./assets/img/casual.jpg";
  photo2 = "./assets/img/graysuit.jpg";
  photo3 = "./assets/img/tuxedo.jpg";
  photo4 = "./assets/img/blackhate.jpg";
  photo5 = "./assets/img/gallery5.jpg";
  photo6 = "./assets/img/gallery6.png";
  photo7 = "./assets/img/gallery7.jpg";
  photo8 = "./assets/img/gallery8.jpg";
  photo9 = "./assets/img/gallery9.png";

  constructor(public navCtrl: NavController, private router: Router) { }

  detail(data){
    this.router.navigate(['/galery-detail',{image:data}]);
  }

  ngOnInit() {
  }

}
