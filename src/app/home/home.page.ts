import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { ThemeService } from '../theme.service';

//contoh tema yang akan diaplikasikan
const themes = {
  autumn: {
    primary: '#F78154',
    secondary: '#4D9078',
    tertiary: '#B4436C',
    light: '#FDE8DF',
    medium: '#FCD0A2',
    dark: '#B89876'
  },
  night: {
    primary: '#8CBA80',
    secondary: '#FCFF6C',
    tertiary: '#FE5F55',
    medium: '#BCC2C7',
    dark: '#F7F7FF',
    light: '#495867'
  },
  neon: {
    primary: '#39BFBD',
    secondary: '#4CE0B3',
    tertiary: '#FF5E79',
    light: '#F4EDF2',
    medium: '#B682A5',
    dark: '#34162A'
  }
};


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  showBtn: boolean = false;
  deferredPrompt;

  sliderConfig = {
    spaceBetween: 10,
    centeredSlides: true,
    slidesPerView: 3.5,
    loop: true
  }

  constructor(private platform: Platform, private router: Router, private theme:ThemeService) {
    // this.ionViewWillEnter();
    // this.add_to_home();
  }

  public isMD =  this.platform.is('android');
  public isIOS =  this.platform.is('ios');

  // ionViewDidLoad(){
  //   this.platform.ready().then(() => {
  //     this.add_to_home();
  //     console.log("Hello, ini ionviewdidload");
  //   });
  // }

  ionViewWillEnter(){
    window.addEventListener('beforeinstallprompt', (e) => {
      // Prevent Chrome 67 and earlier from automatically showing the prompt
      e.preventDefault();
      // Stash the event so it can be triggered later on the button event.
      this.deferredPrompt = e;
       
    // Update UI by showing a button to notify the user they can add to home screen
      
    });
     
    //button click event to show the promt
             
    window.addEventListener('appinstalled', (event) => {
     alert('App Installed');
    });
     
    // if (window.matchMedia('(display-mode: standalone)').matches) {
    //   alert('Your Are Entering Standalone Mode of This App');
    // }
  }

  //Function for show more button in each card categoy in the home
  morearticle(){
    this.router.navigateByUrl('/article');
  }

  moregallery(){
    this.router.navigateByUrl('/galery');
  }

  morevideo(){
    this.router.navigateByUrl('/video');
  }

  changeTheme(name) {
    this.theme.setTheme(themes[name]);
  }

  changeSpeed(val) {
    this.theme.setVariable('--speed', `${val}ms`);
  }


  add_to_home(e){
    debugger
    // hide our user interface that shows our button
    // Show the prompt
    this.deferredPrompt.prompt();
    // Wait for the user to respond to the prompt
    this.deferredPrompt.userChoice
      .then((choiceResult) => {
        // if (choiceResult.outcome === 'accepted') {
        //   alert('User accepted the prompt');
        // }
        this.deferredPrompt = null;
      });
  };
  
}
