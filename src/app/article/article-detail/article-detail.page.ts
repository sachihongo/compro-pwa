import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.page.html',
  styleUrls: ['./article-detail.page.scss'],
})
export class ArticleDetailPage implements OnInit {
  image:any;

  constructor(private router:ActivatedRoute) { 
    this.image = this.router.snapshot.paramMap.get('image');
  }

  ngOnInit() {
  }

}
