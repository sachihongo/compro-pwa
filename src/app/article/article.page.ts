import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-article',
  templateUrl: './article.page.html',
  styleUrls: ['./article.page.scss'],
})
export class ArticlePage implements OnInit {

  abc = [
    {
      name: 'Rozy',
      gender: 'female',
      image: './assets/img/article1.jpg',
      nomor: 1
    },
    {
      name: 'Dave',
      gender: 'male',
      image: './assets/img/article2.jpg',
      nomor: 2
    },
    {
      name: 'Paul',
      gender: 'male',
      image: './assets/img/article3.jpg',
      nomor: 3
    },
    {
      name: 'Maria',
      gender: 'female',
      image: './assets/img/article4.jpg',
      nomor: 4
    },
    {
      name: 'Maria',
      gender: 'female',
      image: './assets/img/article4.jpg',
      nomor: 5
    },
  ];

  count: number = 0;

  // hitung = Object.keys(this.abc).length;

  constructor(public navCtrl: NavController, private router: Router) {
    console.log("hallo");
  }

  ngOnInit() {
  }

  navigate(data) {
    this.router.navigate(['/article-detail', { image: data }]);
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      for (let i = 0; i < 2; i++) {
        this.abc.push(this.abc[this.count]);
        this.count++;
      }
      console.log('Ended async operation');
      infiniteScroll.target.complete();
    }, 500);

  }

}
