import { Injectable } from '@angular/core';
import { AppDataService } from '../app-data/app-data.service';
import { Http, Response } from '@angular/http';
import { map, catchError } from 'rxjs/operators';
import 'rxjs/add/operator/do';
import { Observable } from 'rxjs';
import { FunctionUrlService } from '../functions/function-url.service';

/*
  Generated class for the ServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(
    private urlFunc: FunctionUrlService,
    private http: Http) {
    console.log('Generating ServicesProvider');
  }

  public get(name: string, data: any) {
    return this.http.get(this.urlFunc.getUrl(name, data))
      .pipe(
        map(this.extractData),
         catchError(this.catchError)
       )
  }

  private logResponse(res: Response) {
    console.log(res);
  }

  private extractData(res: Response) {
    return res.json();
  }

  private catchError(error: Response | any) {
    console.log(error);
    return Observable.throw(error.json().error || "Server error");
  }

}
