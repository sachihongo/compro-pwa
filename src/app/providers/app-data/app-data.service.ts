import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppDataService {
  public appId: string = "11";
  public appUuid: string = "cv2_product_E95C2A06759E779473CF";
  private env: string = 'LOCAL';
  public apiBaseUrl: string = this.env == 'LOCAL' ? "  http://apiv2.compro.test/" : (this.env == 'DEV' ? "https://dev-api-v2.compro.co.id/" : "https://api.compro.id/v2/");
}
