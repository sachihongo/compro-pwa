import { Injectable } from '@angular/core';
import { IonApp, MenuController, LoadingController} from '@ionic/angular';
import { AppDataService } from '../app-data/app-data.service';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { SocialSharing, SocialSharingOriginal } from '@ionic-native/social-sharing';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class FunctionsService {

  login: FormGroup;
  main_page: { component: any };
  loading: any;

  constructor(
      public appData: AppDataService,
      public menu: MenuController,
      public app: IonApp,
      public socialSharing: SocialSharingOriginal,
      private router: Router
  ) {
      console.log("Generate FunctionsProvider...");
  }

  // public function
  resolveFunctionName(name: string) {
      if (this.mapFunctions.hasOwnProperty(name)) {
        return this.mapFunctions[name];
      }

      return false;
  }

  execFunction(data: any) {
      let func = this.resolveFunctionName(data.action);
      if (func)
        func(data.data);
      else
        console.log("Not implemented yet!");
  }

  getRandomNumber(min: number, max: number) {
      return Math.floor(Math.random() * (max - min)) + min;
  }

  // end of public function
  // =======================================================

  // private function
  private mapFunctions =
      {
          open_page: (data) => { this.openPage(data); },
          push_page: (data) => { this.pushPage(data); },
          share_article: (data) => { this.shareArticle(data); },
          image_detail: (data) => { this.imageDetail(data); }
      };

  private openPage(data) {
      // close the menu when clicking a link from the menu
      this.menu.close();
      console.log("openPage Called");

      // set nav to created page.
      // this.app.getRootNav().setRoot('MasterPage', { url: data.url, id: data.id, det: data.det });
      this.router.navigate(['/home', { url: data.url, id: data.id, det: data.det }]);
  }

  private pushPage(data) {
      // close the menu when clicking a link from the menu
      this.menu.close();
      console.log("pushPage Called");

      // rootNav is now deprecated (since beta 11) (https://forum.ionicframework.com/t/cant-access-rootnav-after-upgrade-to-beta-11/59889)
      // this.app.getRootNav().setRoot('MasterPage', { url: data.url, id: data.id, det: data.det });
      this.router.navigate(['/home', { url: data.url, id: data.id, det: data.det }]);
  }


  //buat buka detail dari static content seperti gallery dll
  private imageDetail(data){
      console.log("this image detail");
      console.log(data);
  }

  //share article - param: title,content,image url
  private shareArticle(data){
      // Check if sharing via email is supported
      this.socialSharing.canShareViaEmail().then(() => {
      // Sharing via email is possible
          console.log("email bisa");
          // Share via email
          this.socialSharing.shareViaEmail('Body', 'Subject', ['sachi99@gmail.com']).then(() => {
              // Success!
          }).catch(() => {
              // Error!
          });
      }).catch(() => {
          console.log("email tidak bisa");
      // Sharing via email is not possible
      });

      console.log(data);
  }
  
}
