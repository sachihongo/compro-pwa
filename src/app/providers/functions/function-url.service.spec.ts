import { TestBed } from '@angular/core/testing';

import { FunctionUrlService } from './function-url.service';

describe('FunctionUrlService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FunctionUrlService = TestBed.get(FunctionUrlService);
    expect(service).toBeTruthy();
  });
});
