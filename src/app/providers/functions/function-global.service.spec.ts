import { TestBed } from '@angular/core/testing';

import { FunctionGlobalService } from './function-global.service';

describe('FunctionGlobalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FunctionGlobalService = TestBed.get(FunctionGlobalService);
    expect(service).toBeTruthy();
  });
});
