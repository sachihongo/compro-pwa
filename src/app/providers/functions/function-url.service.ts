import { Injectable } from '@angular/core';
import { AppDataService } from '../app-data/app-data.service';

@Injectable({
  providedIn: 'root'
})
export class FunctionUrlService {
  constructor(
    public appData: AppDataService,
  ) {
    console.log("Generate UrlFunctionsProvider...");
  }

  // private function
  private mapFunctions =
    {
      get_app: (data) => { return this.getApp(data); },
      get_menu: (data) => { return this.getMenu(data); },
      get_content: (data) => { return this.getContent(data); }
    };

  public getUrl(name:string, data:any){
    let func = this.resolveFunctionName(name);
    console.log(name);
    return func(data);
  }

  private getApp(data: any){
    let uuid:string = data.uuid;

    return this.appData.apiBaseUrl + 'client/get-app?uuid=' + uuid;
  }

  private getContent(data:any) {
    let url: string = data.url;
    let pageId: string = data.pageId;
    let uuid: string = data.uuid;
    let detailId: string = data.detailId;

    if (detailId != null && detailId != '')
      detailId = '/' + detailId;

    let res = this.appData.apiBaseUrl + 'client/get-content/' + url + '/' + pageId + detailId + '?uuid=' + uuid;
    return res;
  }

  private getMenu(data:any) {
    // getting all app menus.

    let uuid:string = data.uuid;
    return this.appData.apiBaseUrl + 'client/get-menu?uuid=' + uuid;
  }

  resolveFunctionName(name: string) {
    return this.mapFunctions[name] ? this.mapFunctions[name] : null;
  }

}
