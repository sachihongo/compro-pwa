import { Injectable } from '@angular/core';
import { AppDataService } from '../app-data/app-data.service';

@Injectable({
  providedIn: 'root'
})
export class FunctionGlobalService {

  constructor(
    public appData: AppDataService,
  ) {
    console.log("Generate GlobalFunctionsProvider...");
  }

  getRandomNumber(min: number, max: number) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

}
