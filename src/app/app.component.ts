import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: '' /*home*/
    },
    {
      title: 'Articles',
      url: '/article',
      icon: '' /*paper*/
    },
    {
      title: 'Galleries',
      url: '/galery',
      icon: ''
    },
    {
      title: 'Store',
      url: '/store',
      icon: ''
    },
    {
      title: 'Videos',
      url: '/video',
      icon: ''
    },
    {
      title: 'About Us',
      url: '/about',
      icon: ''
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
