import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-video-detail',
  templateUrl: './video-detail.page.html',
  styleUrls: ['./video-detail.page.scss'],
})
export class VideoDetailPage implements OnInit {
  image:any;

  constructor(private router: ActivatedRoute,public navCtrl :NavController) { 
    this.image = this.router.snapshot.paramMap.get('image');
  }

  ngOnInit() {
  }

}
