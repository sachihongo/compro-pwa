import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-video',
  templateUrl: './video.page.html',
  styleUrls: ['./video.page.scss'],
})
export class VideoPage implements OnInit {
  i=0;

  isi = [
          {
            id : 1,
            image : './assets/img/video1.png'
          },
          {
            id : 2,
            image : './assets/img/video2.jpg'
          },
          {
            id : 3,
            image : './assets/img/video3.jpg'
          },
          {
            id : 4,
            image : './assets/img/video3.jpg'
          },
          {
            id : 5,
            image : './assets/img/video3.jpg'
          },
          {
            id : 6,
            image : './assets/img/video3.jpg'
          },
  ];


  constructor(private router: Router, public navCtrl: NavController) { }

  navigate(data){
    this.router.navigate(['/video-detail',{image:data}]);
  }

  ngOnInit() {

  }

}
