import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  { 
    path: 'article',
    loadChildren: './article/article.module#ArticlePageModule'
  },
  { 
    path: 'video',
    loadChildren: './video/video.module#VideoPageModule'
  },
  { 
    path: 'galery',
    loadChildren: './galery/galery.module#GaleryPageModule'
  },
  { 
    path: 'about',
    loadChildren: './about/about.module#AboutPageModule'
  },
  { 
    path: 'galery-detail', 
    loadChildren: './galery/galery-detail/galery-detail.module#GaleryDetailPageModule' 
  },
  { 
    path: 'video-detail', 
    loadChildren: './video/video-detail/video-detail.module#VideoDetailPageModule' 
  },
  { 
    path: 'article-detail', 
    loadChildren: './article/article-detail/article-detail.module#ArticleDetailPageModule' 
  },
  { path: 'store', loadChildren: './store/store.module#StorePageModule' },
  { path: 'cart', loadChildren: './cart/cart.module#CartPageModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
